.PHONY: stats debug info logs restart dialog dbconnect

stats:
	kubectl exec service/sbc -- kamctl stats

debug:
	kubectl exec service/sbc -- kamctl srv debug 3

info:
	kubectl exec service/sbc -- kamctl srv debug 2

sngrep-dev:
	kubectl exec service/sbc-dev -c kamailio -n phones -it -- bash -c "sngrep -I \`ls /tmp/*.pcap | tail -n 1\`"

sngrep-sbc:
	kubectl exec service/sbc -c kamailio -n phones -it -- bash -c "sngrep -I \`ls /tmp/*.pcap | tail -n 1\`"

edit-phones:
	kubectl exec -it service/tftpd -n phones -- bash

logs-dev:
	kubectl logs service/sbc-dev -c kamailio -n phones

logs-sbc:
	kubectl logs service/sbc -c kamailio -n phones

kubens:
	kubens phones

dbconnect:
	kubectl exec service/sbc -it -- kamctl db connect

sbc-cli-backup:
	kubectl exec -it service/sbc -c kamailio-dev -- bash

restart-sbc:
	kubectl rollout restart deployment/sbc -n phones
	kubectl get pod -n phones -w

restart-dev:
	kubectl rollout restart deployment/sbc-dev -n phones
	kubectl get pod -n phones -w

update-dev:
	kubectl apply -f deploy/sbc-config-dev.yaml

dialog-sbc:
	kubectl exec service/sbc -n phones -- kamctl dialog show
