apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: sbc-dev
  namespace: phones
spec:
  secretName: sbc-cert-dev
  dnsNames:
  - sbc-primary-dev.cmu.ca
  - sbc-backup-dev.cmu.ca
  usages:
  - digital signature
  - key encipherment
  - server auth
  - client auth
  issuerRef:
    name: letsencrypt-route53-new-root
    kind: ClusterIssuer

---
apiVersion: v1
kind: Service
metadata:
  name: sbc-dev
  namespace: phones
spec:
  type: ClusterIP
  selector:
    app: sbc-dev
  ports:
  - name: sip-tcp
    port: 5060
    protocol: TCP
    targetPort: 5060
  - name: sips
    port: 5061
    protocol: TCP
    targetPort: 5061

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: sbc-dev
  namespace: phones
spec:
  selector:
    matchLabels:
      app: sbc-dev
  template:
    metadata:
      labels:
        app: sbc-dev
    spec:
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      nodeSelector:
        topology.kubernetes.io/region: shaftesbury
      affinity:
        podAntiAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
          - labelSelector:
              matchExpressions:
              - key: app
                operator: In
                values:
                - sbc-dev
            topologyKey: "kubernetes.io/hostname"
      volumes:
      - name: raw-cfg
        configMap:
          name: sbc-cfg-dev
      - name: processed-cfg
        emptyDir: {}
      - name: sbc-cert
        secret:
          secretName: sbc-cert-dev
      initContainers:
      - name: setup
        image: registry.gitlab.com/cmuwpg/public/kamailio:5.7-cmu1
        command:
        - bash
        - /etc/config/raw/setup.sh
        volumeMounts:
        - name: processed-cfg
          mountPath: /etc/config/final
        - name: raw-cfg
          mountPath: /etc/config/raw
        envFrom:
        - configMapRef:
            name: sbc-cfg-dev-env
        env:
        - name: RTP_KEEPALIVE_PASSWORD
          valueFrom:
            secretKeyRef:
              name: kamailio
              key: RTP_KEEPALIVE_PASSWORD
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              name: kamailio
              key: POSTGRES_PASSWORD
        - name: POSTGRES_RO_PASSWORD
          valueFrom:
            secretKeyRef:
              name: kamailio
              key: POSTGRES_RO_PASSWORD
        - name: RAW
          value: /etc/config/raw
        - name: FINAL
          value: /etc/config/final
      containers:
      - name: kamailio
        image: registry.gitlab.com/cmuwpg/public/kamailio:5.7-cmu1
        volumeMounts:
        - name: processed-cfg
          mountPath: /etc/kamailio
        - name: sbc-cert
          mountPath: /etc/kamailio/certs
        env:
        - name: DEVENV
          value: "true"
        securityContext:
          capabilities:
            add:
            - IPC_LOCK
            - NET_RAW
            - NET_BROADCAST
        readinessProbe:
          exec:
            command:
            - bash
            - -c
            - '(($(kamctl uptime | jq -r ".result.uptime") >= 1))'
          initialDelaySeconds: 5
          timeoutSeconds: 3
          successThreshold: 1
          failureThreshold: 3
      - name: keepalived
        image: registry.gitlab.com/cmuwpg/public/kamailio:5.6-4
        command:
        - /usr/sbin/keepalived
        - --log-console
        - --dont-fork
        volumeMounts:
        - name: processed-cfg
          mountPath: /etc/keepalived
        securityContext:
          capabilities:
            add:
              - NET_ADMIN
              - NET_BROADCAST
              - NET_RAW
