FROM debian:bookworm-slim

MAINTAINER Ryan Rempel <rgrempel@cmu.ca>

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    gnupg wget gettext ca-certificates sngrep keepalived jq sipsak

# kamailio repo
RUN echo "deb http://deb.kamailio.org/kamailio57 bookworm main" > /etc/apt/sources.list.d/kamailio.list
RUN wget -O- http://deb.kamailio.org/kamailiodebkey.gpg | gpg --dearmor > /etc/apt/trusted.gpg.d/kamailio.gpg

RUN apt-get -qq update && apt-get -qq -y --no-install-recommends install \
    kamailio \
    kamailio-dbg \
    kamailio-extra-modules \
    kamailio-ldap-modules \
    kamailio-outbound-modules \
    kamailio-phonenum-modules \
    kamailio-postgres-modules \
    kamailio-presence-modules \
    kamailio-radius-modules \
    kamailio-snmpstats-modules \
    kamailio-tls-modules \
    kamailio-utils-modules \
    kamailio-websocket-modules \
    kamailio-xml-modules

VOLUME /etc/kamailio

ENTRYPOINT ["kamailio", "-DD", "-E"]
